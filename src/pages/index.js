import React from 'react'
import LayoutIndexPage from "../components/layout"
import { getNowData, getPastData, getYestData, getNowStats, getPastStats, updateYestData, updateNowData, updatePastData, makeDateRange, skelNow, skelPast, skelStats, skelNivo } from "../utils/tools"
import NivoLine from "../components/nivo-line"
import CompareNumbers from "../components/compare-numbers"

class Coincap extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      fiats: [ 'USD', 'EUR', 'GBP' ], 
      fiatAllRates: [],
      fiatRates: {
        "GBP": "0.78",
        "USD": "1.00",
        "EUR": "0.92"
      },
      maxCryptos: 100,
      cryptos: [
        { id: 'bitcoin', symbol: 'BTC' },
        { id: 'ethereum', symbol: 'ETH' },
        { id: 'litecoin', symbol: 'LTC' }
      ],
      selectedCrypto: 'BTC',
      ccNow: {},
      nowData: skelNow,
      yestData: skelNow,
      nowStats: skelStats,
      ccPast: {},
      pastData: skelPast,
      pastStats: skelStats,
      nivoData: skelNivo,
      maxRange: 90,
      selectedRange: 30,
      prevRange: 30,
      selectedCurrency: 'USD',
      prevCurrency: 'USD',
    };
    this.currencyOptions = [
      { value: 'USD', label: 'USD' },
      { value: 'EUR', label: 'EUR' },
      { value: 'GBP', label: 'GBP' },
    ];
    this.rangeOptions = [
      { value: 7, label: '7' },
      { value: 30, label: '30' },
      { value: 60, label: '60' },
      { value: 90, label: '90' },
    ];
    this.handleCurrencyChange = this.handleCurrencyChange.bind(this);
    this.handleRangeChange = this.handleRangeChange.bind(this);
    //this.getBpiNow = this.getBpiNow.bind(this);
    //this.getBpiPast = this.getBpiPast.bind(this);
  };

  handleCurrencyChange( updatedCurrency ) {
    this.setState( { 
      prevCurrency: this.state.selectedCurrency,
      selectedCurrency: updatedCurrency,
    });
  }

  handleRangeChange( updatedRange ) {
    this.setState( { 
      prevRange: this.state.selectedRange,
      selectedRange: updatedRange,
    });
  }

  formatCCRates( ccData, fiatRates ) {
    let rates = {};
    ccData.data.map( el => {
      if ( fiatRates.includes ( el.symbol ) )
        rates[ el.symbol ] = (1 / parseFloat( el.rateUsd ) ).toFixed(2);
    });
    return rates;
  }

  
  fetchCCRates = async () => {
    this.setState({
      isLoading: true,
    })
    let requestOptions = {
      method: 'GET',
      redirect: 'follow'
    };
    
    // fiat rates
    await fetch("https://api.coincap.io/v2/rates", requestOptions)
    .then(
      response => response.text()
    )
    .then(result => {
      this.setState({
        isLoading: true,
        ccRates: JSON.parse( result ),
      })
    })
    .then( () => {
      this.setState( {
        fiatRates: this.formatCCRates( this.state.ccRates, this.state.fiats )
      })
      // console.log('rates', this.state.fiatRates);
    })
    .catch( error => console.log('error', error ) );
  }

  fetchCCAssets = async () => {
    this.setState({
      isLoading: true,
    });
    let requestOptions = {
      method: 'GET',
      redirect: 'follow'
    };
    const dateRange = makeDateRange(this.state.maxRange);
    // assets now
      await fetch(`https://api.coincap.io/v2/assets?ids=${this.state.cryptos.map( c => c.id ).toString( ',' )}&limit=${this.state.maxCryptos}`, requestOptions)
      .then( response => response.text() )
      .then( result => {
        this.setState({
          ccNow: JSON.parse( result ),
        })
      })
      .then( () => {
        this.setState( {
          nowData: getNowData( this.state.ccNow.data.find( el => el.symbol === this.state.selectedCrypto ),
          this.state.selectedCurrency,
          this.state.fiatRates
          ),
        })
      // console.log('assets', this.state.nowData);
      })
      .then( async () => {
        await fetch(`https://api.coincap.io/v2/assets/${ this.state.cryptos.find( el => el.symbol === this.state.selectedCrypto ).id }/history?interval=d1&start=${dateRange.start}&end=${dateRange.end}`, requestOptions ) 
      .then( response => response.text() )
      .then( result => {
        this.setState({
          ccYest: JSON.parse( result ),
        })
      })
      .then( () => {
        this.setState( {
          yestData: getYestData( 
            this.state.ccYest.data[this.state.maxRange - 1],
            this.state.selectedCurrency,
            this.state.fiatRates
          ),
        })
        //console.log('yestd', this.state.yestData)
      })
      .then( () => {
        this.setState({
          nowStats: getNowStats(
            this.state.nowData,
            this.state.yestData
          ),
          isLoading: false
        });
      })
      // console.log('nowstats', this.state.nowStats)
    })
    .catch(error => console.log('error', error))
  }

  fetchCCPast = async () => {
    this.setState({
      isLoading: true,
    });
    let requestOptions = {
      method: 'GET',
      redirect: 'follow'
    };
    const dateRange = makeDateRange(this.state.maxRange);
    // history
    await fetch(`https://api.coincap.io/v2/assets/${ this.state.cryptos.find( el => el.symbol === this.state.selectedCrypto ).id }/history?interval=d1&start=${dateRange.start}&end=${dateRange.end}`, requestOptions ) 
    .then( response => response.text() )
    .then( result => {
      this.setState({
        ccPast: JSON.parse( result ),
      })
    })
    .then( () => {
      this.setState( {
        pastData: getPastData(
          this.state.ccPast.data,
          this.state.selectedCurrency,
          this.state.fiatRates,
          this.state.maxRange,
          this.state.selectedRange
        ),
      })
    // console.log('pastD', this.state.pastData);
    })
    .then( () => {
      this.setState({
        pastStats: getPastStats(
          this.state.pastData, 
          this.state.selectedCurrency,
          this.state.fiatRates,
          this.state.maxRange,
          this.state.selectedRange
        ),
        isLoading: false
      });
    // console.log('stats past', this.state.pastStats)
    })
    .catch(error => console.log('error', error))
  }

  async updateCurrencyData() {
    this.setState({
      nowData: updateNowData(
        this.state.nowData,
        this.state.selectedCurrency,
        this.state.fiatRates
      ),
      yestData: updateYestData(
        this.state.yestData,
        this.state.selectedCurrency,
        this.state.fiatRates
      ),
      pastData: updatePastData(
        this.state.pastData,
        this.state.selectedCurrency,
        this.state.fiatRates,
        this.state.maxRange,
        this.state.selectedRange
      ),
    });
  }
  async updateStats() {
    this.setState({
      nowStats: getNowStats(
        this.state.nowData,
        this.state.yestData
      ),
      pastStats: getPastStats(
        this.state.pastData,
        this.state.selectedCurrency,
        this.state.fiatRates,
        this.state.maxRange,
        this.state.selectedRange
      ),
    });
  }


  componentDidMount() {
    this.fetchCCRates();
    this.fetchCCAssets();
    this.fetchCCPast();
  }

  componentDidUpdate() {
    if ( this.state.prevCurrency !== this.state.selectedCurrency ) {
      this.setState({
        prevCurrency: this.state.selectedCurrency,
      });
      this.updateCurrencyData()
      .then(
        this.updateStats()
      )
      .catch( err => console.log('Update error: ', err ))
    };

    if ( this.state.prevRange !== this.state.selectedRange ) {
      this.setState ({
        prevRange: this.state.selectedRange,
        pastData: updatePastData(
          this.state.pastData,
          this.state.selectedCurrency,
          this.state.fiatRates,
          this.state.maxRange,
          this.state.selectedRange
        ),
        pastStats: getPastStats(
          this.state.pastData, 
          this.state.selectedCurrency,
          this.state.fiatRates,
          this.state.maxRange,
          this.state.selectedRange
        ),
      });
    }
  }

  render() {

    const { isLoading, pastData, pastStats, nowData, nowStats, selectedRange, selectedCurrency } = this.state;

    const monthlyRangeTitle = { before: 'Bitcoin Price Last', type: 'range', after: 'Days' };

    const bpiSource = { 
      before: 'Powered by ',
      title: 'CoinCap', 
      url: 'https://www.coincap.io' 
    };

    return (
      <LayoutIndexPage>
        <div className="container">
          <div className="row">
            <CompareNumbers
              type="CompareNumbers"
              data={ nowData.now }
              isLoading={ isLoading } 
              stats={ nowStats }
              rates={ this.state.fiatRates }
              selectedCurrency={ selectedCurrency }
              currencyOptions={ this.currencyOptions }
              currencyHandler={ this.handleCurrencyChange } 
              source={ bpiSource }
              title="Bitcoin Price Now"
            />
          </div>
          <div className="row">
            <NivoLine 
              type="NivoLine"
              inTitle={ monthlyRangeTitle }
              isLoading={ isLoading } 
              data={ pastData.nivo } 
              stats={ pastStats }
              selectedRange={ selectedRange }
              rangeHandler={ this.handleRangeChange }
              rangeOptions={ this.rangeOptions }
              rangePadding={ 500 }
              ticksX="every 8 days"
              ticksY={ 8 }
              source={ bpiSource }
            />
          </div>
        </div>
      </LayoutIndexPage>
    );
  }
}

export default Coincap


