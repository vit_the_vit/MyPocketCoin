// CoinCap tools

export const getNowData = ( ccNow, selCur, rates ) => {
  let coef = Number( rates[ selCur ] );
  let src = {
    "date": new Date().toLocaleDateString(),
    "price": ( parseFloat( ccNow.priceUsd ) * coef ).toFixed(2),
    "id": ccNow.id,
    "rank": ccNow.rank,
    "symbol": ccNow.symbol,
    "name": ccNow.name,
    "supply": parseFloat( ccNow.supply ).toFixed(2),
    "maxSupply": parseFloat( ccNow.maxSupply ).toFixed(2),
    "marketCapUsd": ( parseFloat( ccNow.marketCapUsd ) * coef ).toFixed(2),
    "volumeUsd24Hr": ( parseFloat( ccNow.volumeUsd24Hr ) * coef ).toFixed(2),
    "changePercent24Hr": parseFloat( ccNow.changePercent24Hr ).toFixed(2),
    "vwap24Hr": parseFloat( ccNow.changePercent24Hr ).toFixed(2),
    "fiat": "USD"
  }
  let now = src;
  let toUpdate = [ 'price', 'marketCapUsd', 'volumeUsd24Hr' ];
  for (let p of toUpdate) 
    now[p] = ( now[p] * coef).toFixed(2);
  now.fiat = selCur;
  return {
    now: now,
    src: src
  }
}

export const getYestData = (ccPast, selCur, rates) => {
  let coef = Number( rates[selCur] );

  let src = {
    'date': new Date( ccPast.time ).toLocaleDateString(),
    'price': ( parseFloat( ccPast.priceUsd ) * coef ).toFixed(2)
  };
  return {
    src: src,
    yest: src
  };
}

export const getNowStats = (nowData, yestData ) => {
  let comp = getComp( nowData.now, yestData.yest );  
  let changes = getChanges( comp.latest.price, comp.oldest.price );
  return {
    'lo': comp,
    'ch': changes
  };
}

export const updateNowData = ( nowData, selCur, rates ) => {

  let coef = Number( rates[selCur] );
  let toUpdate = [ 'price', 'marketCapUsd', 'volumeUsd24Hr' ];
  let now = {};
  Object.entries( nowData.src ).map(([key, val]) => {
    if (toUpdate.includes(key))
      now[key] = (val * coef).toFixed(2);
    else
      now[key] = val;
  });
  now['fiat'] = selCur;

  return {
    now: now,
    src: nowData.src
  }
}

export const updateYestData = (nowData, selCur, rates ) => {
  let coef = Number( rates[selCur] );
  return {
    src: nowData.src,
    yest: {
      date: nowData.src.date,
      price: ( parseFloat( nowData.src.price ) * coef ).toFixed(2)  
    }
  };
}

export const getPastData = ( ccPast, selCur, rates, maxRange, selRange ) => {
  let coef = Number( rates[ selCur ] );
  let toSlice = maxRange - selRange;
  let src = ccPast.map( el => ({
    'date': makeDate( el.time , 'date' ),
    'price': ( parseFloat( el.priceUsd ) * coef ).toFixed(2)
  }) );
  let nivo = [{
    id: 'bpi',
    data: ccPast.map( el => ({
      'x': makeDate( el.time, 'date' ),
      'y': ( parseFloat( el.priceUsd ) * coef ).toFixed(2)
    })).slice( toSlice )
  }]
  return {
    src: src,
    nivo: nivo
  }
}

export const updatePastData = (pastData, selCur, rates, maxRange, selRange) => {
  let toSlice = maxRange - selRange;
  let coef = Number( rates[ selCur ] );
  
  let nivo = [{
    id: selCur,
    data: pastData.src.map ( el => ({ 
      x: el.date,
      y: ( parseFloat( el.price ) * coef ).toFixed(2)
    })).slice( toSlice )
  }];
  return {
    nivo: nivo,
    src: pastData.src
  }
};

export const getPastStats = (pastData, selCur, rates, maxRange, selRange) => {
  let toSlice = maxRange - selRange;
  let latestItem = maxRange - 1;
  let oldestItem = maxRange - selRange;
  let coef = Number(rates[selCur]);

  let comp = getComp(pastData.src[latestItem], pastData.src[oldestItem]);

  // min max in range
  let values = [];
  pastData.src.slice(toSlice).map(el => {
    values.push(Number(el.price) * coef);
  });

  let minMax = {
    min: toHundreds( Math.min( ...values ), 'floor' ),
    max: toHundreds( Math.max( ...values ), 'ceil' ),
  };

  // changes
  let changes = getChanges( comp.latest.price, comp.oldest.price );

  return {
    'lo': comp,
    'mm': minMax,
    'ch': changes,
  };
};

// utils

export const getComp = ( l, o ) => ({
  latest: {
    date: l.date,
    price: l.price
  },
  oldest: {
    date: o.date,
    price: o.price 
  }
})

export const getChanges = ( l, o ) => {
  const bigChange = 3; // 3 percent change is big

  let up = false;
  let leap = false;
  let title = '';
  let classes = '';

  let change = percentage( ( l - o ), o ).toFixed(2);

  if ( change > 0 ) { 
    up = true;
    classes = 'up';
    title = 'up';
  } else if ( change === 0 ) {
    classes = 'up'; // intentional
    title = '';
  } else {
    classes = 'down';
    title = 'down';
  };
  if ( change >= bigChange ) {
    leap = true;
    classes += ' leap';
  };
  return { 
    'change': change, 
    'title': title, 
    'classes': classes, 
    'up': up, 
    'leap': leap 
  }; 
};

export const makeDateRange = numDays => {
  let today = new Date();
  let before = new Date( new Date().setDate( new Date().getDate() - numDays - 1 ) );
  return ( {
    'end': Date.parse(today),
    'start': Date.parse(before)
  } ); 
}

export const makeDate = ( str, format ) => {
  let date = new Date( str );
  if ( !Object.prototype.toString.call( date ) === '[object Date]' )
    throw new Error ('Invalid date supplied: ', date);
  if ( format === 'date' ) 
    return date.getFullYear() + "-" + ( '0' + (date.getMonth() + 1 ) ).slice( -2 ) + "-" + ( '0' + date.getDate() ).slice( -2 );
  else 
    return date.toLocaleDateString();
}

export const toHundreds = ( num, roundType = 'round' ) => {
  if ( roundType === 'round' )
    return ( Math.round( num / 100 ) * 100 );
  else if ( roundType === 'floor' )
    return ( Math.floor( num / 100 ) * 100 );
  else if ( roundType === 'ceil' )
    return ( Math.ceil( num / 100 ) * 100 );
}

export const percentage = ( partialValue, totalValue ) => 100 * partialValue / totalValue;

export function isEmpty( obj)  {

  // Speed up calls to hasOwnProperty
  var hasOwnProperty = Object.prototype.hasOwnProperty;
  // null and undefined are "empty"
  if (obj == null) return true;

  // Assume if it has a length property with a non-zero value
  // that that property is correct.
  if (obj.length > 0)    return false;
  if (obj.length === 0)  return true;

  // If it isn't an object at this point
  // it is empty, but it can't be anything *but* empty
  // Is it empty?  Depends on your application.
  if (typeof obj !== "object") return true;

  // Otherwise, does it have any properties of its own?
  // Note that this doesn't handle
  // toString and valueOf enumeration bugs in IE < 9
  for (var key in obj) {
      if (hasOwnProperty.call(obj, key)) return false;
  }
  return true;
}

export const pluralize = (count, noun, suffix = 's') =>
  `${noun}${count !== 1 ? suffix : ''}`;

export const skelStats = {
  lo: {
    latest: {
      date: "na",
      price: "na",
    },
    oldest: {
      date: "na",
      price: "na",
    },
  },
  mm: {
    min: "auto",
    max: "auto",
  },
  ch: {
    change: "NA",
    title: "na",
    classes: "is-loading",
    up: "true",
    leap: "false",
  },
};

export const skelNivo = [ {
  "id": "temp",
  "data": [
    {
      "x": "2020-01-01",
      "y": 7000
    },
    {
      "x": "2020-02-01",
      "y": 9000
    }
  ]
} ];

export const skelPast = {
  nivo: [ {
    "id": "temp",
    "data": [
      {
        "x": "2020-01-01",
        "y": 7000
      },
      {
        "x": "2020-02-01",
        "y": 9000
      }
    ]
  } ],
  src: [
    {
      "date": "2020-01-01",
      "price": 7000
    },
    {
      "date": "2020-02-01",
      "price": 9000
    }
  ]
};


export const skelNow = {
  date: "na",
  price: 0
};
