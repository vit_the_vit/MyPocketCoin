export const getPastDataBPI = ( bpiPast, maxRange, selRange, coef ) => {
  coef = Number( coef );
  let toSlice = maxRange - selRange;
  let src = ccPast.map( el => ({
    'date': makeDate( el.time , 'date' ),
    'price': ( parseFloat( el.priceUsd ) * coef ).toFixed(2)
  }) );
  let nivo = [{
    id: 'bpi',
    data: ccPast.map( el => ({
      'x': makeDate( el.time, 'date' ),
      'y': ( parseFloat( el.priceUsd ) * coef ).toFixed(2)
    })).slice( toSlice )
  }]
  return {
    src: src,
    nivo: nivo
  }
}

export const getNowBPIData = ( bpiNow ) => ({
  date: makeDate( bpiNow.updated ),
  price: parseFloat( bpiNow.rate_float ).toFixed(2)
});

export const getPastBPIData = ( bpiPast, maxRange, selRange, coef ) => {
  let toSlice = maxRange - selRange;

  let nivo = [{
    id: 'bpi',
    data: Object.entries( bpiPast ).map ( ( [x, y] ) => ({ 
      x: makeDate( x, 'date' ),
      y: ( parseFloat( y ) * coef ).toFixed(2)
    }) ).slice( toSlice )
  }];
  let src = Object.entries( bpiPast ).map ( ( [date, price] ) => ({ 
    date: makeDate( date, 'date' ),
    price: ( parseFloat( price ) * coef ).toFixed(2)
  }));
  return {
    nivo: nivo,
    src: src,
  }
};


export const getYestDataBPI = ( bpiPast, coef ) => {
  return Object.entries( bpiPast ).map( ( [date, price] ) => ({ 
    date: makeDate( date, 'date' ),
    price: ( parseFloat( price ) * coef ).toFixed(2)
  }))[0];
};

export const getNowStatsBPI = (nowData, yestData ) => {
  let comp = getComp( nowData, yestData );  
  let changes = getChanges( comp.latest.price, comp.oldest.price );
  return {
    'lo': comp,
    'ch': changes
  };
}
export const makeDateRange = numDays => {
  let today = new Date();
  let before = new Date( new Date().setDate( new Date().getDate() - numDays ) );
  return ( {
    'end': today.toISOString().substring( 0,10 ),
    'start': before.toISOString().substring( 0,10 ),
  } ); 
}

