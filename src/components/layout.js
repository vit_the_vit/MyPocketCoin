import React from "react"
// import SEO from "../components/seo"
import SiteLogo from "../components/site-logo"
import "../css/layout.css"
import "../css/site-logo.css"

const LayoutIndexPage = ({ children }) => {
  
  return (
    <div className="App">
      {/* <SEO /> */}
      <header>
        <div className="container">
          <SiteLogo />
        </div>
      </header>
      <main>
        {children}
      </main>
      <footer>
        © {new Date().getFullYear()}, Built with <a href="https://www.gatsbyjs.org">Gatsby</a>
      </footer>
    </div>
  )
}

export default LayoutIndexPage
