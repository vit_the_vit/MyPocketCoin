import React from 'react';
import Select from 'react-select';

class SelectRange extends React.Component {
  state = {
    selectedOption: this.props.rangeOptions.find( el => el.value === this.props.selectedRange ),
  };
  handleChange = selectedOption => {
    this.setState(
      { selectedOption },
      ( ) => this.props.rangeHandler( selectedOption.value )
    );
  };

  render() {
    const { selectedOption } = this.state;

    return (
      <Select
        value={ selectedOption }
        onChange={ this.handleChange }
        options={ this.props.rangeOptions }
        isClearable={ false }
        isSearchable={ false }
      />
    );
  }
}

export default SelectRange;
