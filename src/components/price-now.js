import React from 'react';

const PriceNow = props => {

  const formatPrice = p => {
    let intPart = Math.floor( Number( p ) );
    let decPart = parseInt( ( p - intPart ).toFixed(2)  * 100 );
    return ({ i: intPart.toString(), d: decPart.toString() });
  }

  let thePrice = {
    i: "na",
    d: "na"
  };

  if (props.price)
    thePrice = formatPrice(props.price);

  return (
    <div className="price-now">
      <span className="int">{ thePrice.i }</span>
      <span className="decimal">.{ thePrice.d }</span>
    </div>
  );

}

export default PriceNow;

