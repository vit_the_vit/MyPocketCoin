import React from 'react';
import Loading from "../components/loading"
import TrendIcon from "../components/trend-icon"
import PriceNow from "../components/price-now"
import SelectFiat from "../components/select-fiat"
import "../css/stats.css"
import "../css/compare-numbers.css"

const CompareNumbers = props => {
  
  const { isLoading, stats, selectedCurrency } = props;

  return (
    <>
      { isLoading ?
        <Loading type={ props.type } />
        :
        <div className="stats CompareNumbers">
          <div className="header CompareNumbers__header">
            <h2>
              { props.title } 
              <span className={ 'details ' + stats.ch.classes  }>
                { stats.ch.change }%
                <TrendIcon up={ stats.ch.up } />
              </span>
            </h2>
          </div>
          <div className="main CompareNumbers__main">
            <div className={'CompareNumbers__current-price ' + stats.ch.classes}>
              <PriceNow price={ props.data.price } />
                <span className="currency">
                  <SelectFiat 
                    selectedCurrency={props.selectedCurrency}
                    currencyHandler={ props.currencyHandler } 
                    currencyOptions={ props.currencyOptions }
                />
              </span>
            </div>
            <div className="CompareNumbers__rest">
              <span>Current price is <strong>{ props.data.price }</strong> { selectedCurrency } as of { props.data.date }. {' '}
                This is <strong>{ stats.ch.change }</strong> % { stats.ch.title } since { stats.lo.oldest.date } when the closing price was <strong>{ stats.lo.oldest.price }</strong> { props.selectedCurrency }. {' '}
              </span>
            </div>
          </div>
          <div className="footer CompareNumbers__footer">
            <span className="data-source">
              { props.source.before} <a href={props.source.url}>{props.source.title}</a> {' '}
            </span>
            { props.selectedCurrency !== 'USD' && 
              <span className="notes">{ props.selectedCurrency} historical prices are converted from USD using current rates.</span>
            }
          </div>
        </div>
      }
    </>
  )
}

export default CompareNumbers;
