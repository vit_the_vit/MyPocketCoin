import React from 'react';
import Select from 'react-select';


class SelectFiat extends React.Component {

  state = {
    selectedOption: {},
    selectedCurrency: '',
  };

  componentDidMount () {
    this.setState( { 
      selectedCurrency: this.props.selectedCurrency,
      selectedOption: this.props.currencyOptions.find( el => el.value === this.props.selectedCurrency ),
    } );
  }

  handleChange = selectedOption => {
    this.setState( { 
      selectedOption,
      selectedCurrency: selectedOption.value,
    }, () => this.props.currencyHandler( this.state.selectedCurrency ) );
  };

  render() {

    return (
      <Select
        value={this.state.selectedOption}
        onChange={ this.handleChange }
        options={this.props.currencyOptions}
        isClearable={ false }
        isSearchable={ false }
      />
    );
  }
}

export default SelectFiat;
