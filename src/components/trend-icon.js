import React from 'react';
import { ArrowUpward, ArrowDownward } from '@material-ui/icons';

const TrendIcon = ( props ) => {
  if ( props.up === true )
    return ( <ArrowUpward /> );
  else 
    return ( <ArrowDownward /> );
}

export default TrendIcon;
