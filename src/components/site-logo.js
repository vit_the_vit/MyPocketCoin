import React from 'react';
import { DonutLarge } from '@material-ui/icons';

const SiteLogo = ( ) => {

  return (
    <>
      <DonutLarge className="site-mark" />
      <h1 className="site-title">MyPocketCoin</h1>
      <p className="site-tagline">Simple Bitcoin stats</p>
    </>
  );
};

export default SiteLogo;
