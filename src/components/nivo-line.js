import React from 'react';
import { ResponsiveLine } from "@nivo/line"
import Loading from "../components/loading"
import TrendIcon from "../components/trend-icon"
import SelectRange from '../components/select-range';
import "../css/stats.css"
import "../css/nivo-line.css"

const NivoLine = (props) => {

  const { isLoading, stats, rangePadding } = props;
  const ticksY = props.ticksY ? props.ticksY : 10;
  const ticksX = props.ticksX ? props.ticksX : 'every day';
  const mm = {
    min: stats.mm.min - rangePadding,
    max: stats.mm.max + rangePadding
  };


  return (
    <>
      { isLoading ?
        <Loading type={ props.type } />
        :
        <div className="stats NivoLine">          
          <div className="header NivoLine__header">
            <h2>
              { props.inTitle ? 
                <Title 
                  inTitle={ props.inTitle } 
                  type={ props.inTitle.type }
                  selectedRange={ props.selectedRange } 
                  rangeHandler={ props.rangeHandler }
                  rangeOptions={ props.rangeOptions }
                /> 
              : 
              props.title } 
              <span className={ stats.ch.classes + ' details' }>
                { stats.ch.change }%
                <TrendIcon up={ stats.ch.up } />
              </span>
            </h2>
          </div>
          <div className="main NivoLine__main">
            <ResponsiveLine 
              data={ props.data }
              curve="linear"
              margin={{ top: 10, right: 10, bottom: 40, left: 40 }}
              enableSlices='x'
              lineWidth={ props.lineWidth ? props.lineWidth : 2 }
              enableArea={ true }
              areaBaselineValue={ mm.min } 
              areaOpacity= { 0.35 }
              axisTop={null}
              axisRight={null}
              // Y
              yScale={{ 
                type: 'linear', 
                min: mm.min, 
                max: mm.max, 
                stacked: false, 
                reverse: false 
              }}
              axisLeft={{
                orient: 'left',
                tickValues: ticksY,
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
              }}
              // X
              xScale={{
                type: 'time',
                format: '%Y-%m-%d',
                precision: 'day',
              }}
              xFormat="time:%Y-%m-%d"
              enableGridX={ false }
              axisBottom={ {
                format: '%b %d',
                tickValues: ticksX,    
                orient: 'bottom',
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
              } }
              // Style
              colors={{ scheme: 'paired' }} 
              pointSize={10}
              pointColor={{ theme: 'background' }}
              pointBorderColor={{ from: 'serieColor' }}
              pointBorderWidth={1}
              pointLabel="y"
              pointLabelYOffset={-12}
              useMesh={true}
            />  
          </div>
          <div className="DataVizNivo__footer">
            <span className="data-source">
              Data source: <a href={ props.source.url }>{ props.source.title }</a>
            </span>
          </div>
        </div>
      }
    </>
  );
}

export default NivoLine;

const Title = ( props ) => {
  return (
    <>
      { props.inTitle.before }
        { props.inTitle.type === 'range' &&
          <span className="range">
          <SelectRange 
            selectedRange={ props.selectedRange } 
            rangeHandler={ props.rangeHandler }
            rangeOptions={ props.rangeOptions }
          />
          </span>
        }
      { props.inTitle.after }
    </>
  );
}
