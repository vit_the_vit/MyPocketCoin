import React from 'react';
import "../css/compare-numbers.css"

const Loading = props => {

  return (
    <div className={`stats ${props.type} is-loading`}>
      <div className={`header ${props.type}__header`}>
        <h2>
          Loading data
        </h2>
      </div>
      <div className={`main ${props.type}__main`}>
        <div>{/* className="CompareNumbers__current-price" */}
          <i></i><i></i><i></i><i></i>
        </div>
      </div>
    </div>
  );
}

export default Loading;

