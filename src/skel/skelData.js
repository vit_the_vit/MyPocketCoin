export const skeletonStats = () => {
  return {
    'newOld': {
      'new': {
        'date': 'na',
        'price': 'na',
      },
      'old': {
        'date': 'na',
        'price': 'na',
      },
    },
    'minMax': {
      'min': 'auto',
      'max': 'auto',
    },
    'changes': {
      'change': 'NA', 
      'title': 'na', 
      'classes': 'is-loading', 
      'up': 'true', 
      'leap': 'false', 
    }
  }
};

